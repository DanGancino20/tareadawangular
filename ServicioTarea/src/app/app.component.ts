import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesService } from './servicios/clientes.service';
import { ListadoClientesComponent } from './component/listado-clientes/listado-clientes.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ListadoClientesComponent],
  providers: [ClientesService],
})
export class AppComponent {
  title = 'ServicioTarea';
}

