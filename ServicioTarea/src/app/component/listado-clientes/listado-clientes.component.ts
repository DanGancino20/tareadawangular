import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../../servicios/clientes.service';
import { Cliente } from '../../interfaz/cliente';

@Component({
  selector: 'app-listado-clientes',
  template: `
    <ul>
      <li *ngFor="let cliente of clientes">{{ cliente.nombre }} {{ cliente.apellido }} ({{ cliente.email }})</li>
    </ul>
  `,
})
export class ListadoClientesComponent implements OnInit {
  clientes: Cliente[] = [];

  constructor(private clientesService: ClientesService) { }

  ngOnInit(): void {
    this.clientes = this.clientesService.obtenerClientes();
  }
}