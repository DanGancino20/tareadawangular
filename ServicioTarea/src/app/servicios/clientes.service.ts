import { Injectable } from '@angular/core';
import { Cliente } from '../interfaz/cliente';

@Injectable()
export class ClientesService {
  private clientes: Cliente[] = [
    { id: 1, nombre: 'Juan', apellido: 'Pérez', email: 'juan.perez@example.com' },
    { id: 2, nombre: 'María', apellido: 'González', email: 'maria.gonzalez@example.com' },
    { id: 3, nombre: 'Pedro', apellido: 'Rodríguez', email: 'pedro.rodriguez@example.com' },
  ];

  obtenerClientes(): Cliente[] {
    return this.clientes;
  }
}
